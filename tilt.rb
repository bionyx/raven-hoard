require 'curb'
require 'yaml'
require 'tilt'
require 'ostruct'
require 'date'
require 'digest'
require 'json'

def to_ostruct(object)
  case object
  when Hash
    OpenStruct.new(Hash[object.map {|k, v| [k, to_ostruct(v)] }])
  when Array
    object.map {|x| to_ostruct(x) }
  else
    object
  end
end

def split(filearg)
  filename=filearg.split('.')[0]
  return filename.split('/')
end

def config()
  site = YAML::load_file('config.yaml')
  return to_ostruct(site )
end

def api_call
  start=Time.now
  puts "BEGIN #{start}"
  c=config()
  cache={}
  c.accounts.each{ |a|
    hoarde={}
    hoarde['identified']={ magic: [], rare: [], unique: []  }
    hoarde['not']={}
    hoarde['money']={ orbs: {}, other: {} }
    hoarde['icons']={}
    hoarde['value']=0.0
    for tab in 1..a.max_tab
      t="https://www.pathofexile.com/character-window/get-stash-items?accountName=#{a.name}&tabIndex=#{tab}&tabs=0&league=#{a.league}&accessKey=#{a.key}"
      puts "api call to: #{t}"
      r = Curl::Easy.perform(t);
      j=JSON.parse(r.body);
      hoarde=quartermaster( hoarde, j["items"] )
    end
    cache = prepare(hoarde, a.name, c)
  }
  return cache
end

def value( name)
  values = {}
  values["Orb: Alchemy"]=  3.575
  values["Orb: Regret"]= 4.704
  values["Orb: Scouring"]=  2.2725
  values["Orb: Chance"]= 0.9
  values["Orb: Fusing"]= 0.9
  values["Orb: Chromatic"]= 0.2816497709
  values["Orb: Jeweller's"]= 0.3095
  values["Orb: Alteration"]= 0.146843318
  values["Orb: Augmentation"]=  0.0279
  values["Orb: Transmutation"]= 0.0072
  values["Orb: Chaos"]=  3.6
  values["Orb: Vaal"]= 2.97
  values["Orb: Eternal"]=  360
  values["Orb: Exalted"]=  144
  values["Orb: Divine"]= 84.6
  values["Orb: Regal"]=  4.5
  values["Orb: Blessed"]=  3.96
  return values[name]
end

def quartermaster( hoarde, items )
  value=[1,2,4,8]
  rare_str=[ nil, :magic, :rare, :unique ]
  for cur in items
    set=[0,0,0,0]
    rarity = cur["frameType"]
    type=cur["typeLine"]
    if rarity<=3
      match = /<<.*>><<.*>><<.*>>(.*)/.match(type)
      if match
        bucket=rare_str[rarity]
        hoarde["identified"][bucket].push(match[1])
      else
        type = type.sub( 'Superior ', '')
        if hoarde['not'][type]
          set=hoarde['not'][type]
        end
        set[rarity] = value[rarity]
        hoarde['not'][type]=set
      end
    elsif rarity == 5
      stack=cur["properties"][0]["values"][0][0].split('/')[0].to_i
      orb = /(.*)Orb( of)?(.*)/.match(type)
      target={}
      if orb
        target=hoarde['money'][:orbs]
        type ="Orb: #{(orb[1]+orb[3]).strip}"
        puts type+"foo"
        puts value(type)
        puts stack
        hoarde['value']+=(stack*value(type))
      else
        match=/(Scroll)?( of )?(.* \b)?(Scroll)?(Shard)?(.*\z)/.match(type)
        if match[1] || match[4]
          type = "Scroll: #{match[3]}#{match[6]}"
        elsif match[5]
          type = "Shard: #{match[3]}#{match[6]}"
        else
          type = "Quality: #{match[3]}#{match[6]}"
        end
        type=type.strip
        target=hoarde['money'][:other]
      end
      if target[type]
        target[type]+=stack
      else
        hoarde['icons'][type]=cur['icon']
        target[type]=stack
      end
    end
  end
  puts hoarde['money']
  puts hoarde['icons']
  return hoarde
end

def prepare( hoarde, account, c )
  tier={}
  cache={ unique: {}, rare: {}, other:{}, identified:{} }
  for i in 0..16
    tier[i]=[]
  end
  for name, set in hoarde['not']
    index=set.reduce(:+)
    tier[index].push(name)
  end
  tier=tier.reject{|k,v| v.length==0}
  for k,v in tier
    target={}
    if k>=8
      target=cache[:unique]
    elsif k>=4
      target=cache[:rare]
    else
      target=cache[:other]
    end
    for item in v
      target[item]=k 
    end
  end
  for k,v in hoarde['identified']
    hoarde['identified'][k]=v.sort
  end
  cache[:identified]=hoarde['identified']
  cache[:money]=hoarde['money']
  cache[:icons]=hoarde['icons']
  cache[:chance]=Integer(hoarde['value']+1)
  cache[:exalted]=0
  while cache[:chance]>=144
    cache[:exalted]+=1
    cache[:chance]-=144
  end
  return cache
end

def update(s)
  structure=split(s)
  changed=structure[3..-1].join('/')
  account_data = api_call()
  context = config()
  c2 = Object.new
  def c2.title
    'raven-hoard'
  end
  template_file_name=context.template.dir+context.template.name
  output_file_name=context.output_dir+changed+".html"
  puts template_file_name
  puts output_file_name
  puts s

  template = Tilt::HamlTemplate.new(template_file_name)
  File.open( output_file_name, "w") do |file|
      file.write template.render( context ) {
          Tilt::HamlTemplate.new( s ).render(c2, account: account_data)
      }
  end
end
s=ARGV[0]
context = config()
print s.sub(context.source_dir,'')
structure=split(s)
if structure[2]=='layouts' then
  print 'layout updated'
  updates=Dir.glob(context.source_dir+'pages/**/*').select{ |e| File.file? e }
  updates.each{ |page|
    print "update #{page}\n"
    update(page)
  }
else 
  update(s)
end
