require 'ostruct'
require 'date'
require 'digest'
require 'yaml'
require 'curb'
require 'json'
require 'sqlite3'
require 'headless'
require 'selenium-webdriver'

def to_ostruct(object)
  case object
  when Hash
    OpenStruct.new(Hash[object.map {|k, v| [k, to_ostruct(v)] }])
  when Array
    object.map {|x| to_ostruct(x) }
  else
    object
  end
end

def config()
  return to_ostruct(YAML::load_file('/home/sinh/code/raven-hoard/config.yaml'))
end

def api_call
  start=Time.now
  puts "BEGIN #{start}"
  c=config()
  c.accounts.each{ |a|
    hoarde={}
    hoarde['identified']={ magic: [], rare: [], unique: []  }
    hoarde['not']={}
    puts hoarde
    for tab in 1..a.max_tab
      t="https://www.pathofexile.com/character-window/get-stash-items?accountName=#{a.name}&tabIndex=#{tab}&tabs=0&league=#{a.league}&accessKey=#{a.key}"
      puts "api call to: #{t}"
      r = Curl::Easy.perform(t);
      j=JSON.parse(r.body);
      hoarde=quartermaster( hoarde, j["items"] )
    end
    store(hoarde, a.name, c)
  }
  end_time=Time.now
  puts "END #{end_time}, #{end_time-start} seconds to process "
end

def quartermaster( hoarde, items )
  value=[1,2,4,8]
  rare_str=[ nil, :magic, :rare, :unique ]
  for cur in items
    set=[0,0,0,0]
    rarity = cur["frameType"]
    if rarity<=3
      type=cur["typeLine"]
      match = /<<.*>><<.*>><<.*>>(.*)/.match(type)
      if match
        bucket=rare_str[rarity]
        hoarde["identified"][bucket].push(match[1])
      else
        type = type.sub( 'Superior ', '')
        if hoarde['not'][type]
          set=hoarde['not'][type]
        end
        set[rarity] = value[rarity]
        hoarde['not'][type]=set
      end
    end
  end
  return hoarde
end

def store( hoarde, account, c )
  tier={}
  store={ unique: {}, rare: {}, other:{}, identified:[] }
  for i in 0..16
    tier[i]=[]
  end
  for name, set in hoarde['not']
    index=set.reduce(:+)
    tier[index].push(name)
  end
  tier=tier.reject{|k,v| v.length==0}
  for k,v in tier
    target={}
    if k>=8
      target=store[:unique]
    elsif k>=4
      target=store[:rare]
    else
      target=store[:other]
    end
    for item in v
      target[item]=k
    end
  end
  for k,v in hoarde['identified']
    hoarde['identified'][k]=v.sort
  end
  store[:identified]=hoarde['identified']
  yam = store.to_yaml
  filename="#{c.output_path}#{account}"
  if File.exists? filename
    File.delete( filename  )
  end
  file = File.open( filename, "w")
  file.puts(yam)
  file.close
end
send( ARGV[0])
